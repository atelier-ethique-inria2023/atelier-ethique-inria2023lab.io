---
title: "Accueil"
description: "Atelier Éthique dans l'ESR - Inria 2023"
date: 2023-06-04
showLicense: true
showToc: true
layout: "homepage"
image: "/img/Logo_Nicomaque_bord-blanc-min.png"
---

<!-- Ltex: language=fr -->


<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=FulRbc9tawrlFEFN&amp;list=PLsrAkSTLO6qlFHBKDtzqNEj09TVE4qm3j" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


## 📜 Résumé


Au regard du dérèglement climatique, pour lequel les activités humaines en sont les causes majoritaires, [l'avis du Comité d'Éthique du CNRS (COMETS) du 5 décembre 2022](https://comite-ethique.cnrs.fr/avis-du-comets-integrer-les-enjeux-environnementaux-a-la-conduite-de-la-recherche-une-responsabilite-ethique/) nous suggère que *« ce sont d’abord certains des fondements et principes éthiques de la recherche qui commandent l’intégration de la dimension environnementale dans la conduite [de la recherche] »*.
Et si cet avis concerne le sujet du climat, les dimensions sociales s'y intègrent indépendamment ou par intrication.

Par conséquent, si l'intégrité scientifique et la commande que l'éthique fait à l'endroit de nos responsabilités, à la fois dans la réflexion sur les applications, [mais aussi sur les implications](https://www.cairn.info/revue-anthropologie-des-connaissances-2019-2-page-381.htm) que peuvent avoir nos travaux sur la planète et la société, alors les chercheur·euses doivent être en capacité de discuter leurs sujets de recherche, de les réorienter, voire parfois de les abandonner.

Toutefois, plusieurs mécanismes, seuls ou en combinaisons, réduisent nos capacités à composer une recherche éthique et s'opposent aux [libertés académiques](https://rogueesr.fr/liberte-academique/) -- légitimées par la pratique éthique et l'intégrité scientifique.
Par exemple, d'un point de vue structurel, les financements pérennes se réduisent au profit de financements fléchés dont les finalités sont posées et postulées.
Également, les établissements d'enseignement et de recherche sont mis en compétition, et les finalités « d'excellence » définissent des évaluations qui peuvent freiner les initiatives de recherches et d'enseignements.

Au travers de cet atelier, nous soulèverons les problématiques suivantes :
* Quelles responsabilités portent l'Enseignement Supérieur et la Recherche (ESR) dans la réflexion éthique ?
* Quelles sont les occasions qui nous poussent à la réflexion éthique dans notre travail au quotidien ?
* Quelles sont nos capacités d'actions sur notre travail de recherche et d'enseignement ?

<!--more-->


## Les présentations

* `0:00:00` Introduction : éthique scientifique, applications et implications
* `0:09:16` Présentation d'Enka BLANCHARD
* `0:14:23` Enka BLANCHARD : la noblesse et le gâteau
* `0:55:26` Présentation de Bernard FRIOT
* `0:57:41` Bernard FRIOT : de l'infra-emploi au statut du fonctionnaire, nécessaire pour de l'éthique en science


## Les questions de la partie débat

* `0:00:00` Introduction
* `0:01:00` Bien que démontrées inefficaces, pourquoi certaines politiques de la recherche sont maintenues et encouragées ?
* `0:11:21` QUID des initiatives de publications en éditions diamants comme PCI ?
* `0:16:42` Qu'est-ce que des critiques ouvertes d'articles scientifiques ? Comment établir les niveaux de qualifications pour le salaire à la personne ?
* `0:27:06` Pourquoi hiérarchiser les qualifications pour le salaire à la personne ?
* `0:35:09` Quelles sont les raisons d'une évaluation continue si elle n'a pas d'impact sur le statut de la personne ?
* `0:38:07` Doit-on arrêter les recherches technologiques quand elles alimentent l'économie capitaliste ?
* `0:51:26` Encadrement des doctorant·es : faut-il des normes pour s'assurer de la responsabilité des encadrant·es ?
* `0:57:12` Trajet en avion et consommation d'énergie pour la recherche : faut-il montrer l'exemple ?
* `1:00:45` QUID d'une sécurité sociale du numérique ?
* `1:07:49` Fin et remerciements


## 🪧 Programme


**Conférence**
* 15:00 – Introduction de l’atelier
* 15:10 – Intervention [Enka BLANCHARD](https://www.koliaza.com/fr/)
* 15:50 – Intervention [Bernard FRIOT](https://fr.wikipedia.org/wiki/Bernard_Friot_(sociologue))

**Pause**
* 16:30 à 17:00 – Collations dans l’espace amphithéâtre de l’Inria

**Place au débat**
* 17:00 à 18:00 – Participation du public


## 🎫 Le Rendez vous


<img src="/img/affiche_portrait.png" alt="Affiche format portrait de l'atelier" width="300">

🗓️ Le 04 juillet 2023

⌚️ **Rendez-vous à 14:45** | 15:00 à 16:30 (conférence), 17:00 à 18:00 (débat)

🗺️ [Centre Inria de l'Université de Rennes](https://www.inria.fr/fr/centre-inria-universite-rennes)

**Inscription (obligatoire, gratuite)**
* 🎫 [Inscription en présence (autre que doctorant·es)](https://www.billetweb.fr/atelier-ethique-esr-inria-2023)
* 🎓 [Formation Amethis (doctorant·es), code ETH-CDB-07](https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formations)
* 📺 [Inscription visioconférence](https://framaforms.org/atelier-ethique-dans-lesr-inria-2023-1686569023)


## 🎓 Formation doctorale


Cet atelier est reconnu comme formation à l'Éthique au profit des doctorant·es par le Collège doctoral de Bretagne (3 heures de formation).
Inscrivez-vous seulement via la formation [Amethis](https://amethis.doctorat-bretagneloire.fr/amethis-client/formation/gestion/formations) s'il vous plait
* **Intitulé de la formation :** Débat Éthique et écologie transdisciplinaire Inria
* **code AMETHIS :** `ETH-CDB-07`


## 🎥 Droit à l'image et enregistrement


À l'attention de celleux qui souhaiteraient intervenir dans le débat / questions :

L'atelier sera filmé et retransmis après montage sur YouTube. Les questions et ou remarques qui seront posées en ligne sont susceptibles d'être enregistrées et ou retransmises. Par défaut, les interventions du public seront anonymisées dans la mesure du possible. Si vous consentez, votre voix ainsi que votre nom peuvent apparaître au montage. Pour cela, vous devrez signaler jusqu'au 11 juillet que vous étiez l'auteur·rice de la dite question / remarque.


## 🗺️ Accès à l'amphithéâtre de l'Inria


<img src="/img/InriaRBA.jpg" alt="Entrée de l'amphithéâtre de l'Inria" height="400">

<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/en/map/inria-acces-amphitheatre_926625?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&captionMenus=true"></iframe><p><a href="//umap.openstreetmap.fr/en/map/inria-acces-amphitheatre_926625">(Voir en plein écran)</a></p>