---
title: "Qui sommes-nous ?"
description: "Les organisateurices"
date: 2023-06-04
showLicense: false
showToc: true
image: "/img/Logo_Nicomaque_bord-blanc-min.png"
---

<!-- Ltex: language=fr -->

[<img src="/img/Logo_Nicomaque_bord-blanc-min.png" alt="Logo de l'association Nicomaque" height="150">](https://www.nicomaque-bretagne.fr/)

L'atelier est une initiative de l'association des doctorant·es et docteur·es bretons [Nicomaque](https://www.nicomaque-bretagne.fr/).

<!--more-->

## ✉️ Les doctorant·es de contact

* [Victor Epain](vepain.gitlab.io)
* [Ambre Ayats](https://scholar.google.com/citations?user=WCl29bEAAAAJ)


## 👥 Iels nous aident à l'organisation

* [Groupe SenS Inria](https://sens-rennes.gitlabpages.inria.fr/)
* [Séminaire éthique IRMAR](https://irmar.univ-rennes.fr/seminars?f%5B0%5D=seminar_type%3A288)
* [ÉPolAr](https://epolar.hypotheses.org/)

Avec :
* [Nicolas Montavont](https://cv.hal.science/nicolas-montavont)
* [Matthieu Simonin](https://people.irisa.fr/Matthieu.Simonin/)
* [Simon Castellan](https://iso.mor.phis.me/)
* [Matthieu Romagny](https://perso.univ-rennes1.fr/matthieu.romagny/)

Merci à Baptiste Gaultier pour la conception de l'affiche.


## 🏛️ Les organismes qui nous soutiennent

<img src="/img/inr_logo_rouge.png" alt="Logo Inria" height="50">
<img src="/img/logo_irisa_cmjn_fondblanc_0.jpg" alt="Logo IRISA" height="50">
<img src="/img/UNIRENNES_LOGOnoir.jpg" alt="Logo Université de Rennes" height="50">
<img src="/img/college_doctoral_bretagne.png" alt="Logo Collège Doctoral de Bretagne" height="55">
<img src="/img/CNRS-LogoPNG1.png" alt="Logo CNRS" height="60">

