---
title: "Rechercher"
description: "Rechercher sur le site"
url: "/rechercher/"
---

You can change search page details above.

Keep this file safe to ensure Hugo generates the search page.
