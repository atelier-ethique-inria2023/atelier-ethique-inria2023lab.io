---
title: "Ressources"
description: "Bibliographie et diverses liens"
date: 2023-06-04
showLicense: false
showToc: true
# TODO image for og
# image: "/img/IMG_20220323_164434.jpg"
---

<!-- Ltex: language=fr -->


## De nos intervenant·es

* 📰 [Recherche et dogmatisme : de l’improductivité du productivisme (2022) - Enka Blanchard](https://www.cairn.info/revue-questions-de-communication-2022-2-page-255.htm)
* 🎙️ [Bernard Friot : et pourquoi pas la retraite à 50 ans ? France Culture](https://www.youtube.com/watch?v=7bc3VVukW1g)


## Du monde de la recherche

* 📰 [Avis du COMETS 2022-12-12](https://comite-ethique.cnrs.fr/avis-du-comets-integrer-les-enjeux-environnementaux-a-la-conduite-de-la-recherche-une-responsabilite-ethique/)
* 💰 [PEPR Inria-INRAe Agroécologie Numérique](https://www.inria.fr/fr/france-2030-pepr-agroecologie-transition-numerique)
* 📗 [La croissance verte contre la nature, Hélène Tordjman](https://www.editionsladecouverte.fr/la_croissance_verte_contre_la_nature-9782348067990)
* 📰 [Penser l’indissociabilité de l’éthique de la recherche, de l’intégrité scientifique et de la responsabilité sociale des sciences. Clarification conceptuelle, propositions épistémologiques - Léo Coutellec](https://www.cairn.info/revue-anthropologie-des-connaissances-2019-2-page-381.htm)
* 🦉 [Collectif RogueESR](https://rogueesr.fr/)
* 👥 [GDR RO - GdT RO et Éthique](http://gdrro.lip6.fr/?q=node/227)
* 👥 [GDR IM - GT Scalp : motion pour des créneaux de discussions politiques](https://www.irif.fr/gt-scalp/journees-2022/motion-17-02-2023)


## Société

* 🎙️ [Faut-il arrêter la recherche ? Science Friction Mediapart](https://www.mediapart.fr/journal/culture-et-idees/030223/faut-il-arreter-la-recherche)
* 🎙️ [Des mandarins aux managers : ceux qui dirigent la recherche en France. Science Friction Mediapart](https://www.mediapart.fr/journal/culture-et-idees/130123/des-mandarins-aux-managers-ceux-qui-dirigent-la-recherche-en-france)
* 🗞️ [La robotique au service de l’agroécologie : une fausse évidence ?](https://blogs.mediapart.fr/atelier-decologie-politique-de-toulouse/blog/070223/la-robotique-au-service-de-l-agroecologie-une-fausse-evidence)

<!--more-->